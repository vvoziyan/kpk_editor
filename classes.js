class Canvas {
    constructor(toolsArea, paramsArea, editorArea, config) {
        this.DOM = document.createElement('div');
        this.initDOM(config.canvas);

        this.viewboxWrap = document.createElement('div');
        this.viewboxWrap.addEventListener('click', (e) => {
            if (e.target == this.viewboxWrap) {
                this.removeMovebaleFromTarget();

            }
        });


        this.viewboxWrap.className = 'viewbox_wrap';
        this.viewboxWrap.appendChild(this.DOM);

        editorArea.appendChild(this.viewboxWrap);
        this.toolsArea = toolsArea;

        this.paramsArea = paramsArea;
        this.blockParamsArea = document.createElement('div');
        this.blockPositioningParamsArea = document.createElement('div');
        this.paramsArea.appendChild(this.blockParamsArea);
        this.paramsArea.appendChild(this.blockPositioningParamsArea);


        this.editorArea = editorArea;
        this.editorArea.addEventListener('click', (e) => {
            if (e.target == this.editorArea) {
                this.removeMovebaleFromTarget();
            }
        });


        this.scale = 1;

        this.blocks = [];

        this.canvasConfig = {
            "width": config.canvas.width,
            "height": config.canvas.height
        }

        this.DOM.style.width = this.canvasConfig.width;
        this.DOM.style.height = this.canvasConfig.height;


        this.moveableConfig = {
            // If you want to use a group, set multiple targets(type: Array<HTMLElement | SVGElement>).
            // target: document.getElementById("target1"),

            draggable: true,
            resizable: true,
            rotatable: true,
            elementGuidelines: [],

            container: this.DOM,

            rootContainer: document.body,
            throttleDrag: 0,
            throttleDragRotate: 0,
            throttleRotate: 0.1,
            startDragRotate: 0,
            zoom: 1,
            origin: true,
            edges: true,
            renderDirections: ["n", "nw", "ne", "s", "se", "sw", "e", "w"],
            padding: {"left": 0, "top": 0, "right": 0, "bottom": 0},
            // bounds: { left: 0, right: config.canvas.width, top: 0, bottom: config.canvas.height }, // DEV
            snappable: true,
            // verticalGuidelines: [0, 100, 200, 300, 400, 500, 600],
            // horizontalGuidelines: [0, 50, 100, 150, 200, 250, 300, 350, 400],
        };

        this.moveable = new Moveable(this.DOM, this.moveableConfig);

        this.positioningInputs = this.createPositioningInputs();

        config.blocks.forEach(blockCfg => {
            this.createBlock(blockCfg)
        })

        this.initViewportScale();

        this.initKeyBindings();


        this.blocks.forEach(block => {
            if (block.blockClass.recalculate) {
                block.blockClass.recalculate();
            }
        })
    }

    initDOM(config) {
        this.DOM.className = "canvas";
        this.DOM.style.width = `${config.width}px`;
        this.DOM.style.height = `${config.height}px`;

        this.DOM.addEventListener('click', (e) => {
            if (e.target == this.DOM) {
                this.removeMovebaleFromTarget()
            }
        });
    }

    initKeyBindings() {
        document.addEventListener('keydown', (e) => {
            if (e.shiftKey) {
                this.moveable.keepRatio = true;
            }
            if (e.ctrlKey) {
                this.moveable.snappable = false;
            }
        })

        document.addEventListener('keyup', (e) => {
            if (e.key == 'Shift') {
                this.moveable.keepRatio = false;
            }
            if (e.key == 'Control') {
                this.moveable.snappable = true;
            }
        })
    }

    removeMovebaleFromTarget() {
        this.moveable.target = undefined;
        this.moveable.off();
        this.moveable.updateRect();

        this.blockParamsArea.innerHTML = '';
        this.blockPositioningParamsArea.innerHTML = '';

    }

    getSize() {

        let style = window.getComputedStyle(this.DOM);
        return [parseInt(style.width), parseInt(style.height)]
    }

    setCanvasWidth(width) {
        this.DOM.style.width = `${width}px`;
        // this.moveable.bounds.right = parseInt(width); // DEV
    }

    setCanvasHeight(height) {
        this.DOM.style.height = `${height}px`;
        // this.moveable.bounds.bottom = parseInt(height); //DEV
    }

    initViewportScale() {
        this.editorArea.onwheel = (e) => {
            if (e.ctrlKey) {
                e.preventDefault();

                this.scale += e.deltaY * -0.001;

                this.scale = Math.min(Math.max(.125, this.scale), 4);

                this.moveable.zoom = 1 / this.scale;

                this.viewboxWrap.style.transform = `scale(${this.scale})`;
            }
        }
    }

    createPositioningInputs() {
        const widthInput = document.createElement('input');
        widthInput.type = 'number';

        const heightInput = document.createElement('input');
        heightInput.type = 'number';

        const rotate = document.createElement('input');
        rotate.type = 'number';

        const xInput = document.createElement('input');
        xInput.type = 'number';

        const yInput = document.createElement('input');
        yInput.type = 'number';

        const moveUpButton = document.createElement('button');
        moveUpButton.innerText = 'Move Up';

        const moveDownButton = document.createElement('button');
        moveDownButton.innerText = 'Move down';

        const deleteButton = document.createElement('button');
        deleteButton.innerText = 'Delete';

        return {
            'width': widthInput,
            'height': heightInput,
            'rotate': rotate,
            'left': xInput,
            'top': yInput,
            'up': moveUpButton,
            'down': moveDownButton,
            'delete': deleteButton
        }
    }

    initPositioningInput(targetBlock) {

        this.blockPositioningParamsArea.appendChild(this.positioningInputs.width);
        this.positioningInputs.width.value = targetBlock.getSize()[0];
        this.positioningInputs.width.oninput = () => {
            targetBlock.DOM.style.width = `${this.positioningInputs.width.value}px`;
            this.moveable.updateRect();
        }

        this.blockPositioningParamsArea.appendChild(this.positioningInputs.height);
        this.positioningInputs.height.value = targetBlock.getSize()[1];
        this.positioningInputs.height.oninput = () => {
            targetBlock.DOM.style.height = `${this.positioningInputs.height.value}px`;
            this.moveable.updateRect();
        }

        this.blockPositioningParamsArea.appendChild(this.positioningInputs.left);
        this.positioningInputs.left.value = targetBlock.frame.translate[0];
        this.positioningInputs.left.oninput = () => {
            targetBlock.frame.translate[0] = parseInt(this.positioningInputs.left.value);
            targetBlock.DOM.style.transform = `translate(${targetBlock.frame.translate[0]}px, ${targetBlock.frame.translate[1]}px) rotate(${targetBlock.frame.rotate}deg)`
            this.moveable.updateRect();
        }

        this.blockPositioningParamsArea.appendChild(this.positioningInputs.top);
        this.positioningInputs.top.value = targetBlock.frame.translate[1];
        this.positioningInputs.top.oninput = () => {
            targetBlock.frame.translate[1] = parseInt(this.positioningInputs.top.value);
            targetBlock.DOM.style.transform = `translate(${targetBlock.frame.translate[0]}px, ${targetBlock.frame.translate[1]}px) rotate(${targetBlock.frame.rotate}deg)`
            this.moveable.updateRect();
        }

        this.blockPositioningParamsArea.appendChild(this.positioningInputs.rotate);
        this.positioningInputs.rotate.value = targetBlock.frame.rotate;
        this.positioningInputs.rotate.oninput = () => {
            targetBlock.frame.rotate = parseInt(this.positioningInputs.rotate.value);
            targetBlock.DOM.style.transform = `translate(${targetBlock.frame.translate[0]}px, ${targetBlock.frame.translate[1]}px) rotate(${targetBlock.frame.rotate}deg)`

            this.moveable.updateRect();
            this.moveable.updateTarget();
        }

        this.blockPositioningParamsArea.appendChild(this.positioningInputs.up);
        this.positioningInputs.up.onclick = () => {
            this.moveBlockUp(targetBlock);
        }

        this.blockPositioningParamsArea.appendChild(this.positioningInputs.down);
        this.positioningInputs.down.onclick = () => {
            this.moveBlockDown(targetBlock);
        }

        this.blockPositioningParamsArea.appendChild(this.positioningInputs.delete);
        this.positioningInputs.delete.onclick = () => {
            this.removeBlock(targetBlock);
        }
    }

    createBlock(blockConfig) {
        const newBlock = new Block(blockConfig);
        this.DOM.appendChild(newBlock.DOM);


        this.blockParamsArea.innerHTML = '';
        if (newBlock.blockClass.inputs) {
            Object.keys(newBlock.blockClass.inputs).forEach(inputKey => {
                this.blockParamsArea.appendChild(newBlock.blockClass.inputs[inputKey]);
            })
        }

        this.setMoveable(newBlock);
        this.initPositioningInput(newBlock);


        newBlock.DOM.addEventListener('click', () => {
            this.blockParamsArea.innerHTML = '';


            if (newBlock.blockClass.inputs) {
                Object.keys(newBlock.blockClass.inputs).forEach(inputKey => {
                    this.blockParamsArea.appendChild(newBlock.blockClass.inputs[inputKey]);
                })
            }

            this.setMoveable(newBlock);
            this.initPositioningInput(newBlock);
        })

        this.blocks.push(newBlock);

    }

    setMoveable(targetBlock) {
        this.moveable.target = targetBlock.DOM;
        this.moveable.updateTarget();
        this.moveable.off();

        this.moveable.on("dragStart", e => {
            let guideLines = [];

            this.blocks.forEach((block) => {
                guideLines.push(block.DOM);
            });

            this.moveable.elementGuidelines = guideLines;

            e.set(targetBlock.frame.translate);
            e.dragStart && e.dragStart.set(targetBlock.frame.translate);
        }).on("drag", e => {
            targetBlock.frame.translate = e.beforeTranslate;
            // e.target.style.transform = `translate(${e.beforeTranslate[0]}px, ${e.beforeTranslate[1]}px)`;

            this.positioningInputs.left.value = e.beforeTranslate[0];
            this.positioningInputs.top.value = e.beforeTranslate[1];

        }).on("rotateStart", e => {
            e.set(targetBlock.frame.rotate);
        }).on("rotate", e => {
            targetBlock.frame.rotate = e.beforeRotate;
            this.positioningInputs.rotate.value = targetBlock.frame.rotate;
        }).on("resizeStart", e => {
            e.setOrigin(["%", "%"]);
        }).on("resize", e => {
            const beforeTranslate = e.drag.beforeTranslate;
            targetBlock.frame.translate = beforeTranslate;

            e.target.style.width = `${e.width}px`;
            e.target.style.height = `${e.height}px`;

            this.positioningInputs.width.value = e.width;
            this.positioningInputs.height.value = e.height;

        }).on("render", e => {
            const {translate, rotate} = targetBlock.frame;
            e.target.style.transform = `translate(${translate[0]}px, ${translate[1]}px)`
                + ` rotate(${rotate}deg)`;
            if (targetBlock.blockClass.recalculate) {
                targetBlock.blockClass.recalculate();
            }
        });

    }

    moveBlockUp(block) {
        const currentLayer = block.layer;
        let switchWith = undefined;
        Object.keys(this.blocks).forEach(elem => {
            if (this.blocks[elem].layer == currentLayer + 1) {
                switchWith = this.blocks[elem];
            }
        });

        if (switchWith) {
            block.layer += 1;
            block.DOM.style.zIndex = block.layer;
            switchWith.layer -= 1;
            switchWith.DOM.style.zIndex = switchWith.layer;

        }
    }

    moveBlockDown(block) {
        const currentLayer = block.layer;
        let switchWith = undefined;
        Object.keys(this.blocks).forEach(elem => {
            if (this.blocks[elem].layer == currentLayer - 1) {
                switchWith = this.blocks[elem];
            }
        });

        if (switchWith) {
            block.layer -= 1;
            block.DOM.style.zIndex = block.layer;
            switchWith.layer += 1;
            switchWith.DOM.style.zIndex = switchWith.layer;

        }
    }

    getMaxLayer() {
        let maxLayer = 0;
        Object.keys(this.blocks).forEach(elem => {
            maxLayer = Math.max(maxLayer, this.blocks[elem].layer);
        });
        return maxLayer;
    }

    removeBlock(block) {
        this.blockParamsArea.innerHTML = '';
        this.blockPositioningParamsArea.innerHTML = '';

        const idx = this.blocks.indexOf(block);
        const layer = block.layer;

        this.moveable.target = undefined;
        this.moveable.off();
        this.moveable.updateRect();

        this.DOM.removeChild(this.blocks[idx].DOM);
        this.blocks.splice(idx, 1);

        Object.keys(this.blocks).forEach(elem => {
            if (this.blocks[elem].layer > layer) {
                this.blocks[elem].layer--;
                this.blocks[elem].DOM.style.zIndex = this.blocks[elem].layer;

            }
        });
    }

    retrieveConfig() {
        let cfg = {};
        cfg.canvas = {
            'width': this.getSize()[0],
            'height': this.getSize()[1]
        }

        cfg.blocks = [];

        Object.keys(this.blocks).forEach(elem => {
            cfg.blocks.push(
                this.blocks[elem].retrieveConfig()
            )
        });

        return cfg
    }
}

class Block {
    constructor(config) {
        const blockTypes = {
            'Rectangle': BlockRectangle,
            'Text': BlockText,
            'Image': BlockImage,
            'Oval': BlockOval,
            'Counter': BlockCounter,
            'ScrollText': BlockScrollText
        }

        this.DOM = document.createElement('div');
        this.DOM.classList = ['block'];
        this.DOM.style.width = '100px';
        this.DOM.style.height = '100px';

        this.layer = 1;
        this.frame = {};
        this.config = config;

        this.blockClass = new blockTypes[config.type]();

        this.DOM.appendChild(this.blockClass.DOM);

        this.applyConfig();
    }

    getSize() {

        let style = window.getComputedStyle(this.DOM);
        return [parseInt(style.width), parseInt(style.height)]
    }

    applyConfig() {
        this.frame.translate = [this.config.wrap.left, this.config.wrap.top];
        this.frame.rotate = this.config.wrap.rotate;

        this.DOM.style.transform = `translate(${this.frame.translate[0]}px, ${this.frame.translate[1]}px) rotate(${this.frame.rotate}deg)`;

        this.layer = this.config.wrap.zIndex;
        this.DOM.style.zIndex = this.config.wrap.zIndex;

        this.DOM.style.width = `${this.config.wrap.width}px`;
        this.DOM.style.height = `${this.config.wrap.height}px`;

        this.blockClass.applyConfig(this.config.child);
    }

    retrieveConfig() {
        let cfg = {}
        cfg.wrap = {
            'left': this.frame.translate[0],
            'top': this.frame.translate[1],
            'width': this.getSize()[0],
            'height': this.getSize()[1],
            'rotate': this.frame.rotate,
            'zIndex': this.layer
        };

        cfg.child = this.blockClass.retrieveConfig();

        cfg.type = this.blockClass.type;

        return cfg
    }
}