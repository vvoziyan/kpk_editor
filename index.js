const loadLayer = document.querySelector('.load__wrap');


let initCfg = {}

async function app() {
    const requestOptions = {
        method: 'GET',
        redirect: 'follow',
        cache: 'no-store',
        headers: {
            "Accept": "application/json"
        }
    };

    await fetch("http://68.183.5.55:8000/getConfig/011", requestOptions)
        .then((response) => { console.log(1111, response.type); return response.text() })
        .then((result) => {
            console.log(result);
            initCfg = JSON.parse(result);
        })
        .catch(error => console.log('error', error));




    const toolsArea = document.querySelector('.tools');
    const paramsArea = document.querySelector('.params');
    const editorArea = document.querySelector('.editor');


    let canv = new Canvas(toolsArea, paramsArea, editorArea, initCfg);
    // document.querySelector('.editor').appendChild(canv.viewboxWrap);

    document.body.addEventListener('wheel', (e) => {
        e.preventDefault();
    })




    const bCreateRectangle = document.createElement('button');
    bCreateRectangle.innerText = 'Create rectangle';
    bCreateRectangle.addEventListener('click', (e) => {
        canv.createBlock({
            'type': 'Rectangle',
            'wrap': {
                'left': 0,
                'top': 0,
                'width': 100,
                'height': 100,
                'rotate': 0,
                'zIndex': canv.getMaxLayer() + 1
            },
            'child': {
                'backgroundColor': '#dddddd',
                'opacity': 1
            }
        })
    })
    toolsArea.appendChild(bCreateRectangle);

    const bCreateText = document.createElement('button');
    bCreateText.innerText = 'Create text';
    bCreateText.addEventListener('click', (e) => {
        canv.createBlock({
            'type': 'Text',
            'wrap': {
                'left': 0,
                'top': 0,
                'width': 250,
                'height': 100,
                'rotate': 0,
                'zIndex': canv.getMaxLayer() + 1
            },
            'child': {
                'text': 'Sample text',
                'fontSize': 30,
                'style': {
                    'color': '#555555'
                }
            }
        })
    })
    toolsArea.appendChild(bCreateText);

    const bCreateImage = document.createElement('button');
    bCreateImage.innerText = 'Create image';
    bCreateImage.addEventListener('click', (e) => {
        canv.createBlock({
            'type': 'Image',
            'wrap': {
                'left': 0,
                'top': 0,
                'width': 250,
                'height': 250,
                'rotate': 0,
                'zIndex': canv.getMaxLayer() + 1
            },
            'child': {
                "imageUrl": "imagePlaceholder.png"
            }
        })
    })
    toolsArea.appendChild(bCreateImage);

    const bCreateOval = document.createElement('button');
    bCreateOval.innerText = 'Create oval';
    bCreateOval.addEventListener('click', (e) => {
        canv.createBlock({
            'type': 'Oval',
            'wrap': {
                'left': 0,
                'top': 0,
                'width': 150,
                'height': 150,
                'rotate': 0,
                'zIndex': canv.getMaxLayer() + 1
            },
            'child': {
                'backgroundColor': '#dddddd',
                'opacity': 1
            }
        })
    })
    toolsArea.appendChild(bCreateOval);

    const bCreateCounter = document.createElement('button');
    bCreateCounter.innerText = 'Create counter';
    bCreateCounter.addEventListener('click', (e) => {
        canv.createBlock({
            'type': 'Counter',
            'wrap': {
                'left': 0,
                'top': 0,
                'width': 150,
                'height': 150,
                'rotate': 0,
                'zIndex': canv.getMaxLayer() + 1
            },
            'child': {
                'text': 'Sample text',
                'fontSize': 30,
                'style': {
                    'color': '#555555'
                }
            }
        })
    })
    toolsArea.appendChild(bCreateCounter);

    const bCreateScrollBanner = document.createElement('button');
    bCreateScrollBanner.innerText = 'Create scroll banner';
    bCreateScrollBanner.addEventListener('click', (e) => {
        canv.createBlock({
            'type': 'ScrollText',
            'wrap': {
                'left': 0,
                'top': 0,
                'width': 150,
                'height': 150,
                'rotate': 0,
                'zIndex': canv.getMaxLayer() + 1
            },
            'child': {
                'text': 'Sample text',
                'fontSize': 30,
                'loopTime': 20,
                'style': {
                    'color': '#555555'
                }
            }
        })
    })
    toolsArea.appendChild(bCreateScrollBanner);

    const canvasWidthInput = document.createElement('input');
    canvasWidthInput.value = initCfg.canvas.width;
    canvasWidthInput.addEventListener('input', (e) => {
        canv.setCanvasWidth(canvasWidthInput.value)
    })
    toolsArea.appendChild(canvasWidthInput);

    const canvasHeightInput = document.createElement('input');
    canvasHeightInput.value = initCfg.canvas.height;
    canvasHeightInput.addEventListener('input', (e) => {
        canv.setCanvasHeight(canvasHeightInput.value)
    })
    toolsArea.appendChild(canvasHeightInput);

    const sendConfigButton = document.createElement('button');
    sendConfigButton.innerText = 'Save';
    sendConfigButton.addEventListener('click', async (e) => {
        loadLayer.className = 'load__wrap';
        await sendConfig();
        loadLayer.className = 'load__wrap hidden';
    })
    toolsArea.appendChild(sendConfigButton);


    loadLayer.className = 'load__wrap hidden';

    // sendConfig();

    async function sendConfig() {
        var formdata = new FormData();
        formdata.append("item", JSON.stringify(canv.retrieveConfig()));

        var requestOptions = {
            method: 'POST',
            body: formdata,
            redirect: 'follow'
        };

        await fetch("http://68.183.5.55:8000/items/011", requestOptions)
            .then(response => response.text())
            .then(result => console.log(result))
            .catch(error => console.log('error', error));
    }
}

app();
