class BlockRectangle {
    constructor() {
        this.type = 'Rectangle';

        this.DOM = document.createElement('div');
        this.DOM.style.width = '100%';
        this.DOM.style.height = '100%';
        this.DOM.style.backgroundColor = '#000000';

        this.inputs = {
            'backgroundColor': (() => {
                const input = document.createElement('input');
                input.setAttribute('type', 'color');

                input.addEventListener('input', (e) => {
                    this.setColor(e);
                })

                return input;
            })(),
            'borderRadius': (() => {
                const input = document.createElement('input');
                input.setAttribute('type', 'range');
                input.setAttribute('min', '0');
                input.setAttribute('step', '0.01');
                input.setAttribute('max', '1');

                input.addEventListener('input', (e) => {
                    this.setBorderRadius(e);
                })

                return input;
            })(),
            'opacity': (() => {
                const input = document.createElement('input');
                input.setAttribute('type', 'range');
                input.setAttribute('min', '0');
                input.setAttribute('max', '1');
                input.setAttribute('step', '0.01');

                input.addEventListener('input', (e) => {
                    this.setOpacity(e);
                })

                return input;
            })(),
        };
    }

    applyConfig(config) {
        console.log(config);
        if (config.backgroundColor) {
            console.log(1111);
            this.DOM.style.backgroundColor = config.backgroundColor;
            this.inputs.backgroundColor.value = config.backgroundColor;
            
        }
        
        if (config.borderRadius) {
            this.inputs.borderRadius.value = config.borderRadius;
            this.DOM.style.borderRadius = `${0.5*this.inputs.borderRadius.value*Math.min(this.getSize()[0], this.getSize()[1])}px`;
        }

        if (config.opacity) {
            this.DOM.style.opacity = config.opacity;
            this.inputs.opacity.value = config.opacity;
        }

    }

    retrieveConfig() {
        let config = {};

        let style = window.getComputedStyle(this.DOM);
        let color = style.backgroundColor;

        if (color[0] == 'r') {
            color = this.convertRGBtoHex(color);
        }

        config.opacity = this.inputs.opacity.value;

        config.backgroundColor = color;

        config.borderRadius = this.inputs.borderRadius.value;

        return config;

    }

    getSize() {

        let style = window.getComputedStyle(this.DOM);
        console.log(style.width);
        return [parseInt(style.width), parseInt(style.height)]
    }

    setBorderRadius(e) {
        e = e || window.event;
        this.DOM.style.borderRadius = `${0.5*e.target.value*Math.min(this.getSize()[0], this.getSize()[1])}px`;
    }

    setColor(e) {
        e = e || window.event;
        if (e.target.value[0] == 'r') {
            this.DOM.style.backgroundColor = this.convertRGBtoHex(e.target.value);
        }
        else {
            this.DOM.style.backgroundColor = e.target.value;
        }
    }

    setOpacity(e) {
        e = e || window.event;
        this.DOM.style.opacity = e.target.value; 
    }

    recalculate() {
        this.DOM.style.borderRadius = `${0.5*this.inputs.borderRadius.value*Math.min(this.getSize()[0], this.getSize()[1])}px`;
    }

    getDOM() {
        return this.DOM;
    }

    colorToHex(color) {
        var hexadecimal = parseInt(color).toString(16);
        return hexadecimal.length == 1 ? "0" + hexadecimal : hexadecimal;
    }
    
    convertRGBtoHex(rgb) {
        rgb = rgb.replace(/[^\d,]/g, '').split(',');

        console.log(rgb);
        return "#" + this.colorToHex(rgb[0]) + this.colorToHex(rgb[1]) + this.colorToHex(rgb[2]);
    }
}

