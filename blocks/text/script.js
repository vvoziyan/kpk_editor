class BlockText {
    constructor() {
        this.type = 'Text';

        this.DOM = document.createElement('p');


        // this.DOM.style.whiteSpace = 'nowrap';
        this.DOM.style.padding = '0';
        this.DOM.style.margin = '0';
        this.DOM.style.width = '100%';
        this.DOM.style.height = '100%';
        this.DOM.style.textAlign = 'center';
        this.DOM.style.verticalAlign = 'middle';
        this.DOM.style.display = 'flex';
        this.DOM.style.justifyContent = 'center';
        this.DOM.style.alignItems = 'center';


        this.inputs = {
            'text': (() => {
                const input = document.createElement('input');
                input.setAttribute('type', 'text');

                input.addEventListener('input', (e) => {
                    this.setText(e);
                })

                return input;
            })(),
            'color': (() => {
                const input = document.createElement('input');
                input.setAttribute('type', 'color');

                input.addEventListener('input', (e) => {
                    this.setColor(e);
                })

                return input;
            })(),
            'fontSize': (() => {
                const input = document.createElement('input');
                input.setAttribute('type', 'number');

                input.addEventListener('input', (e) => {
                    this.setFontSize(e);
                })

                return input;
            })(),
        };
    }

    applyConfig(config) {
        if (config.text) {
            this.DOM.innerHTML = config.text;
            this.inputs.text.value = config.text;
        }

        if (config.fontSize) {
            this.DOM.style.fontSize = `${config.fontSize}px`;
            this.inputs.fontSize.value = config.fontSize;
        }


        if (config.style) {
            if (config.style.color) {
                this.DOM.style.color = config.style.color;
                this.inputs.color.value = config.style.color;
            };
        }
    }

    
    retrieveConfig() {
        let config = {};
        config.text = this.DOM.innerText;
        
        let style = window.getComputedStyle(this.DOM);
        let color = style.color
        
        if (style.color[0] == 'r') {
            color = this.convertRGBtoHex(style.color);
        }

        config.fontSize = this.inputs.fontSize.value;
        
        config.style = {
            'color': color,
        }
        
        return config;
        
    }
    
    setFontSize(e) {
        e = e || window.event;
        this.DOM.style.fontSize = `${e.target.value}px`;
    }

    setText(e) {
        e = e || window.event;
        this.DOM.innerText = e.target.value;
    }

    setColor(e) {
        e = e || window.event;
        if (e.target.value[0] == 'r') {
            this.DOM.style.color = this.convertRGBtoHex(e.target.value);
        }
        else {
            this.DOM.style.color = e.target.value;
        }
    }

    setFontSize(e) {
        e = e || window.event;
        this.DOM.style.fontSize = String(e.target.value) + "px";
    }

    getDOM() {
        return this.DOM;
    }

    colorToHex(color) {
        var hexadecimal = parseInt(color).toString(16);
        return hexadecimal.length == 1 ? "0" + hexadecimal : hexadecimal;
    }

    convertRGBtoHex(rgb) {
        rgb = rgb.replace(/[^\d,]/g, '').split(',');

        console.log(rgb);
        return "#" + this.colorToHex(rgb[0]) + this.colorToHex(rgb[1]) + this.colorToHex(rgb[2]);
    }
}

