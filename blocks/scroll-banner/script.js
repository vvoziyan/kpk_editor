class BlockScrollText {
    constructor() {
        this.type = 'ScrollText';

        this.DOM = document.createElement('section');
        this.DOM.className = "_block__scroll_banner_right";
        
        this.textDOM = document.createElement('p');
        this.DOM.appendChild(this.textDOM);

        // this.DOM.style.whiteSpace = 'nowrap';


        this.inputs = {
            'text': (() => {
                const input = document.createElement('input');
                input.setAttribute('type', 'text');

                input.addEventListener('input', (e) => {
                    this.setText(e);
                })

                return input;
            })(),
            'color': (() => {
                const input = document.createElement('input');
                input.setAttribute('type', 'color');

                input.addEventListener('input', (e) => {
                    this.setColor(e);
                })

                return input;
            })(),
            'fontSize': (() => {
                const input = document.createElement('input');
                input.setAttribute('type', 'number');

                input.addEventListener('input', (e) => {
                    this.setFontSize(e);
                })

                return input;
            })(),
            'loopTime': (() => {
                const input = document.createElement('input');
                input.setAttribute('type', 'number');

                input.addEventListener('input', (e) => {
                    this.setAnimationTime(e);
                })

                return input;
            })(),
        };
    }

    applyConfig(config) {
        if (config.text) {
            this.textDOM.innerHTML = config.text;
            this.inputs.text.value = config.text;
        }

        if (config.fontSize) {
            this.textDOM.style.fontSize = `${config.fontSize}px`;
            this.inputs.fontSize.value = config.fontSize;
        }

        if (config.loopTime) {
            this.textDOM.style.animationDuration = `${config.loopTime}s`;
            this.inputs.loopTime.value = config.loopTime;
        }

        if (config.style) {
            if (config.style.color) {
                this.textDOM.style.color = config.style.color;
                this.inputs.color.value = config.style.color;
            };
        }
    }

    
    retrieveConfig() {
        let config = {};
        config.text = this.textDOM.innerText;
        
        let style = window.getComputedStyle(this.textDOM);
        let color = style.color
        
        if (style.color[0] == 'r') {
            color = this.convertRGBtoHex(style.color);
        }

        config.fontSize = this.inputs.fontSize.value;
        
        config.loopTime = this.inputs.loopTime.value;

        config.style = {
            'color': color,
        }
        
        return config;
        
    }
    
    setAnimationTime(e) {
        e = e || window.event;
        this.textDOM.style.animationDuration = `${e.target.value}s`;
    }

    setFontSize(e) {
        e = e || window.event;
        this.textDOM.style.fontSize = `${e.target.value}px`;
    }

    setText(e) {
        e = e || window.event;
        this.textDOM.innerText = e.target.value;
    }

    setColor(e) {
        e = e || window.event;
        if (e.target.value[0] == 'r') {
            this.textDOM.style.color = this.convertRGBtoHex(e.target.value);
        }
        else {
            this.textDOM.style.color = e.target.value;
        }
    }

    setFontSize(e) {
        e = e || window.event;
        this.textDOM.style.fontSize = String(e.target.value) + "px";
    }

    getDOM() {
        return this.DOM;
    }

    colorToHex(color) {
        var hexadecimal = parseInt(color).toString(16);
        return hexadecimal.length == 1 ? "0" + hexadecimal : hexadecimal;
    }

    convertRGBtoHex(rgb) {
        rgb = rgb.replace(/[^\d,]/g, '').split(',');

        console.log(rgb);
        return "#" + this.colorToHex(rgb[0]) + this.colorToHex(rgb[1]) + this.colorToHex(rgb[2]);
    }
}

