class BlockImage {
    constructor() {
        this.type = 'Image';

        // this.imageUrl = '';

        this.DOM = document.createElement('img');

        this.DOM.style.width = '100%';
        this.DOM.style.height = '100%';
        this.DOM.style.display = 'block';
        
        this.DOM.style.objectFit = 'fill';

        this.inputs = [
            (() => {
                const input = document.createElement('input');
                input.setAttribute('type', 'file');
                input.setAttribute('accept', 'image/png, image/jpeg');

                input.addEventListener('input', (e) => {
                    this.setImage(e);
                })

                return input;
            })(),
        ];
    }

    applyConfig(config) {
        if (config.imageUrl) {
            this.DOM.src = config.imageUrl;
        }
    }

    retrieveConfig() {
        let config = {};
        config.imageUrl = this.DOM.src;

        return config;

    }

    setImage(e) {
        e = e || window.event;

        const file = e.target.files[0];

        if (file) {
            const reader = new FileReader();

            const DOM = this.DOM;

            reader.addEventListener('load', function () {
                // console.log(this)
                DOM.setAttribute('src', this.result);

            });

            reader.readAsDataURL(file);

        }
    }

    recalculate() {

    }

    getDOM() {
        return this.DOM;
    }
}

