class BlockCounter {
    constructor() {
        this.type = 'Counter';

        this.DOM = document.createElement('p');


        // this.DOM.style.whiteSpace = 'nowrap';
        this.DOM.style.padding = '0';
        this.DOM.style.margin = '0';
        this.DOM.style.width = '100%';
        this.DOM.style.height = '100%';
        this.DOM.style.textAlign = 'center';
        this.DOM.style.verticalAlign = 'middle';
        this.DOM.style.display = 'flex';
        this.DOM.style.justifyContent = 'center';
        this.DOM.style.alignItems = 'center';


        this.inputs = {
            'color': (() => {
                const input = document.createElement('input');
                input.setAttribute('type', 'color');

                input.addEventListener('input', (e) => {
                    this.setColor(e);
                })

                return input;
            })(),
            'fontSize': (() => {
                const input = document.createElement('input');
                input.setAttribute('type', 'number');

                input.addEventListener('input', (e) => {
                    this.setFontSize(e);
                })

                return input;
            })(),
            'targetTime': (() => {
                const input = document.createElement('input');
                input.setAttribute('type', 'time');

                input.addEventListener('input', (e) => {
                    this.setTargetTime(e);
                })

                return input;
            })(),
            'targetDate': (() => {
                const input = document.createElement('input');
                input.setAttribute('type', 'date');

                input.addEventListener('input', (e) => {
                    this.setTargetTime(e);
                })

                return input;
            })(),
            'timeUnit': (() => {
                const input = document.createElement('select');
                // input.setAttribute('type', 'number');
                const o1 = document.createElement('option');
                o1.innerText = 'Seconds';
                o1.value = 'sec';
                input.appendChild(o1)

                const o2 = document.createElement('option');
                o2.innerText = 'Minutes';
                o2.value = 'min';
                input.appendChild(o2)

                const o3 = document.createElement('option');
                o3.innerText = 'Hours';
                o3.value = 'hour';
                input.appendChild(o3)

                const o4 = document.createElement('option');
                o4.innerText = 'Days';
                o4.value = 'day';
                input.appendChild(o4)

                input.addEventListener('input', (e) => {
                    this.setFontSize(e);
                })

                return input;
            })(),
        };


        // this.inputs.targetDate.value = '2022-02-20';
        // this.inputs.targetTime.value = '10:00';
        this.countDownTarget = new Date(this.inputs.targetDate.value + ' ' + this.inputs.targetTime.value);
        this.initCounter();


    }

    applyConfig(config) {
        if (config.fontSize) {
            this.DOM.style.fontSize = `${config.fontSize}px`;
            this.inputs.fontSize.value = config.fontSize;
        }
        else {
            this.DOM.style.fontSize = `${30}px`;
            this.inputs.fontSize.value = 30;
        }
        
        if (config.timeUnit) {
            this.inputs.timeUnit.value = config.timeUnit;
        }

        if (config.targetTime) {
            this.inputs.targetTime.value = config.targetTime;
        }
        
        if (config.targetDate) {
            this.inputs.targetDate.value = config.targetDate;
        }
        this.setTargetTime();


        if (config.style) {
            if (config.style.color) {
                this.DOM.style.color = config.style.color;
                this.inputs.color.value = config.style.color;
            };
        }
    }

    setTargetTime() {
        this.countDownTarget = new Date(this.inputs.targetDate.value + ' ' + this.inputs.targetTime.value);
    }

    retrieveConfig() {
        let config = {};

        let style = window.getComputedStyle(this.DOM);
        let color = style.color

        if (style.color[0] == 'r') {
            color = this.convertRGBtoHex(style.color);
        }

        config.fontSize = this.inputs.fontSize.value;

        config.targetTime = this.inputs.targetTime.value;
        config.targetDate = this.inputs.targetDate.value;
        config.timeUnit = this.inputs.timeUnit.value;

        config.style = {
            'color': color,
        }

        return config;

    }

    setFontSize(e) {
        e = e || window.event;
        this.DOM.style.fontSize = `${e.target.value}px`;
    }

    setColor(e) {
        e = e || window.event;
        if (e.target.value[0] == 'r') {
            this.DOM.style.color = this.convertRGBtoHex(e.target.value);
        }
        else {
            this.DOM.style.color = e.target.value;
        }
    }

    initCounter() {
        const second = 1000,
            minute = second * 60,
            hour = minute * 60,
            day = hour * 24;


        let x = setInterval(() => {

            const now = new Date().getTime();
            let distance = this.countDownTarget - now;

            let days = Math.max(Math.floor(distance / (day)), 0);
            let hours = Math.max(Math.floor((distance % (day)) / (hour)), 0);
            let minutes = Math.max(Math.floor((distance % (hour)) / (minute)), 0);
            let seconds = Math.max(Math.floor((distance % (minute)) / second), 0);
            
            // console.log(String(hours).length);

            if (String(days).length < 2) {
                days = '0' + String(days);
            }
            if (String(hours).length < 2) {
                hours = '0' + String(hours);
            }
            if (String(minutes).length < 2) {
                minutes = '0' + String(minutes);
            }
            if (String(seconds).length < 2) {
                seconds = '0' + String(seconds);
            }
            
            if (this.inputs.timeUnit.value == 'sec') {
                this.DOM.innerText = seconds;
            }
            else if (this.inputs.timeUnit.value == 'min') {
                this.DOM.innerText = minutes;
            }
            if (this.inputs.timeUnit.value == 'hour') {
                this.DOM.innerText = hours;
            }
            if (this.inputs.timeUnit.value == 'day') {
                this.DOM.innerText = days;
            }

            // document.getElementById("days").innerText = days;
            // document.getElementById("hours").innerText = hours;
            // document.getElementById("minutes").innerText = minutes;
            // document.getElementById("seconds").innerText = seconds;
        }, 100)
    }

    setFontSize(e) {
        e = e || window.event;
        this.DOM.style.fontSize = String(e.target.value) + "px";
    }

    getDOM() {
        return this.DOM;
    }

    colorToHex(color) {
        var hexadecimal = parseInt(color).toString(16);
        return hexadecimal.length == 1 ? "0" + hexadecimal : hexadecimal;
    }

    convertRGBtoHex(rgb) {
        rgb = rgb.replace(/[^\d,]/g, '').split(',');

        console.log(rgb);
        return "#" + this.colorToHex(rgb[0]) + this.colorToHex(rgb[1]) + this.colorToHex(rgb[2]);
    }
}

